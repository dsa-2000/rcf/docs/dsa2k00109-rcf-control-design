#! /bin/bash
pdfname=D2k-00109-RCF-DES-Control_Design.pdf

pandoc docs/*.md -o ${pdfname} -s -V colorlinks -V links-as-notes --number-sections --template doc-templates/pandoc/template.latex --include-in-header docs/header.tex --bibliography docs/bibliography.bib --csl=docs/ieee.csl --citeproc
