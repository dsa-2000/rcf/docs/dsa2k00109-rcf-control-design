# Appendix: FSM monitoring points
\label{app:fsm-monitoring}

\begin{table*}[htbp]
\centering
\footnotesize
\begin{tabular}{p{0.2\textwidth} p{0.13\textwidth} p{0.13\textwidth} p{0.13\textwidth} p{0.13\textwidth} p{0.1\textwidth}}
\toprule
\textbf{Description} & \textbf{Elements} & \textbf{Size (bytes)} & \textbf{Cadence (s)} & \textbf{Datarate (bytes/s)} & \textbf{Type}\\
\midrule
Chassis temp & 3 & 12 & 30 & 0.4 & push \\ 
Chassis voltage & 3 & 12 & 30 & 0.4 & push \\ 
Chassis fan state & 3 & 12 & 30 & 0.4 & push \\ 
\bottomrule
\end{tabular}
\caption{FSM chassis monitoring points}
\label{tab:fsm-monitoring}
\end{table*}

\begin{table*}[htbp]
\centering
\footnotesize
\begin{tabular}{p{0.2\textwidth} p{0.13\textwidth} p{0.13\textwidth} p{0.13\textwidth} p{0.13\textwidth} p{0.1\textwidth}}
\toprule
\textbf{Description} & \textbf{Elements} & \textbf{Size (bytes)} & \textbf{Cadence (s)} & \textbf{Datarate (bytes/s)} & \textbf{Type}\\
\midrule
Temperature & 3 & 12 & 30 & 0.4 & push \\
Voltage & 10 & 40 & 30 & 1.33333 & push \\
Current & 10 & 40 & 30 & 1.33333 & push \\
firmware version number & 1 & 32 & 30 & 1.06667 & push \\
firmware build time & 1 & 32 & 30 & 1.06667 & push \\
FPGA clock & 1 & 4 & 30 & 0.13333 & push \\
uptime & 1 & 4 & 1 & 4 & push \\
period\_fpga\_clocks & 1 & 4 & 1 & 4 & push \\
period\_variations & 1 & 4 & 1 & 4 & push \\
sync\_count & 1 & 4 & 1 & 4 & push \\ 
telescope time & 1 & 8 & 1 & 8 & push \\
\bottomrule
\end{tabular}
\caption{FPGA card monitoring points}
\label{tab:fpga-monitoring}
\end{table*}

\begin{table*}[htbp]
\centering
\footnotesize
\begin{tabular}{p{0.21\textwidth} p{0.1\textwidth} p{0.1\textwidth} p{0.1\textwidth} p{0.1\textwidth} p{0.19\textwidth}}
\toprule
\textbf{Description} & \textbf{Elements} & \textbf{Size (bytes)} & \textbf{Cadence (s)} & \textbf{Datarate (bytes/s)} & \textbf{Type}\\
\midrule
adc\_stats	                & 3	    & 12	&  1	& 12	    & push\\
adc\_snapshot	            & 16384	& 32768	&  60	& 546.13333	& push / ondemand\\
adc\_histogram	            & 4096	& 16384	&  60	& 273.06667	& push / ondemand\\
input switch state	        & 1	    & 1	    &  60	& 0.016667	& push / onchange\\
noise\_seed	                & 1	    & 4	    &  60	& 0.06667	& push / onchange\\
coarse delay	            & 1	    & 4	    &  60	& 0.06667	& push / onchange\\
fft\_shift0	                & 1	    & 4	    &  60	& 0.06667	& push / onchange\\
fft\_shift1	                & 1	    & 4	    &  60	& 0.06667	& push / onchange\\
fft\_shift\_zoom\_a	        & 1	    & 4 	&  60	& 0.06667	& push / onchange\\
fft\_shift\_zoom\_b	        & 1	    & 4	    &  60	& 0.06667	& push / onchange\\
fft\_overflow\_count0	        & 1	    & 4	    &  1	& 4	        & push\\
fft\_overflow\_count1	        & 1	    & 4	    &  1	& 4	        & push\\
fft\_overflow\_count\_zoom\_a	& 1	    & 4	    &  1	& 4	        & push\\
fft\_overflow\_count\_zoom\_b	& 1	    & 4	    &  1	& 4	        & push\\
Autocorrelation0	        & 2048	& 8192	&  60	& 136.53333	& push / ondemand\\
Autocorrelation1	        & 8192	& 32768	&  60	& 546.13333	& push / ondemand\\
Autocorrelation zoom A	    & 4096	& 16384	&  60	& 273.06667	& push / ondemand\\
Autocorrelation zoom B	    & 2048	& 8192	&  60	& 136.53333	& push / ondemand\\
PFB0 EQ coefficients	    & 2048	& 8192	&  300	& 27.306667	& push / onchange\\
Other coefficients	        & 16384	& 65536	&  300	& 218.45333	& push / onchange\\
Channel selection PFB0	    & 2	    & 8	    &  300	& 0.026667	& push / onchange\\
Channel destinations	    & 15000	& 60000	&  300	& 200	    & push / onchange\\
Ethernet data rate	        & 1	    & 4	    &  1	& 4	        & push\\
Ethernet packet rate	    & 1	    & 4	    &  1	& 4	        & push\\
Main tracking delay/phases	& 4	    & 16	&  1	& 16	    & push\\
Beam tracking delay/phases	& 16	& 64	&  1	& 64	    & push\\
\bottomrule
\end{tabular}
\caption{Signal monitoring points.}
\label{tab:signal-monitoring}
\end{table*}

\normalsize