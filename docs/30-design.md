# Design 
\label{sec:design}

In designing the control and monitoring system for RCF, we considered two main approaches with some variations on each.

The first option is a distributed approach, where each FSM runs its own control process largely in isolation. Each process interfaces with MNC, determines the FSM's position in the signal processing chain and determines what actions it should be performing. One of the chief benefits of this approach is resiliency, as there is no central point of failure and each FSM will keep operating as instructed in a best-effort manner. When FSMs are turned on, they will determine what they need to do and start operating immediately without need for further supervision. Since the telescope will operate largely in one continuous survey mode,  the control processes operating on each FSM can be optimised to cater to it, reducing complexity. This first approach does come with several drawbacks, however:

\begin{itemize}
    \item Metadata requests will be duplicated across all 2048 FSMs, increasing load on MNC (especially on startup, when such requests would be near-simultaneous).
    \item Corralling FSMs into separate groups of antennas will be harder to achieve reliably and might require a central coordinating process anyway.
    \item It will be more complex to implement a unified and unchanging API for RCF, especially when it comes to requirements such as system self-tests.
    \item Updates to metadata delivery and retrieval will necessarily involve installing updates across the entire fleet of FSMs.
    \item While the standard mode of operation will account for the vast majority of telescope operations, there are still a number of other observing modes that must be supported. Handling such observing modes individually and in isolation, FSM by FSM with no coordinating process, adds complexity to the processes.
\end{itemize}

The second option is more centralised and mitigates these downsides. Each individual FSM runs a simpler local process with much more limited autonomy. These processes are directed at a high level by a centralised "coordinator" process, which is also responsible for high-level interactions with MNC (e.g. reporting overall RCF status and responding to self-test requests).

This more centralised approach brings with it several benefits. The distributed processes running on each of the modules can be kept relatively simple, with straightforward state transitions. High-level complexity can then be confined to the coordinator process, including mapping between central telescope states (as listed in Table 1 of [@sys-des]) and global RCF states.

Since the coordinator has full control over these processes, it is easy to avoid and correct potential failure modes in which (for example) some subset of the modules fail to transition to the correct state. And, given that certain state transitions are initiated and controlled by the central coordinator, it can be ensured that such transitions take place in unison at the correct time. Central control over startup procedures ensures that all the FSMs and SRMs power on and start their various processes in the correct sequence. These latter advantages are particularly appealing given the distributed manner in which beamforming will take place (Fig. 10, in [@rcf-des]).

An obvious downside to the introduction of a central coordinator process is the creation of a single point of failure. However, such risks are mitigated through careful design and by providing each individual module a limited degree of intrinsic autonomy.

Ultimately this second approach was adopted, where each FSM runs an individual control process of limited complexity and autonomy which is directed by a central ``coordinator" process.

## Metadata

RCF will need to handle several types of metadata and other data (all data not delivered via the high-speed network to RCP). These are, broadly:

\begin{enumerate}

\item Metadata that must be continuously updated, delivered at a regular cadence or upon change. Examples include delay polynomials (delivered once per second) or antenna pointing coordinates (which update upon changing).

\item Observation configuration parameters which are set at the beginning of an observation, for example the specific participating antennas (and associated resources).

\item Internal RCF system configuration parameters (such as FSM configuration parameters) that generally remain unchanged except in the case of development work or maintenance.

\item Firmware updates (for e.g. the FSMs). These are relatively large (of the order 30MB) and are expected to be delivered comparatively rarely. They may need to be delivered via an alternative mechanism to the standard MNC transport layer which the other three types will use.

\end{enumerate}

Where possible, the RCF control and monitoring system has been designed to limit the volume of requests made by the different processes. For example, delay polynomials are only requested once per second, with faster updates (at around 1kHz) calculated by local delay control modules.

For metadata requests that are the same across large numbers of FSMs and SRMs, the coordinator makes a single set of requests and then distributes the results to each of the modules. This has the added benefit that, should changes to the MNC interface be made, RCF need only make changes to the coordinator in response.

Table \ref{tab:metadata-requirements} summarises the proposed basic metadata that should be made available via MNC transport. Immediate update indicates that any changes should be transmitted to RCF immediately when they take place. The data from the sources in the "Likely Origin" column are transmitted via MNC transport.

\begin{table*}[htbp]
\centering
\footnotesize
\begin{tabular}{P{0.13\textwidth} P{0.4\textwidth} P{0.2\textwidth} P{0.13\textwidth} P{0.13\textwidth}}
\toprule
\textbf{Metadata} & \textbf{Description} & \textbf{Cadence}\footnotemark[1] & \textbf{Type} & \textbf{Likely Origin}\\
\midrule
Antenna ID & The ID of the antenna connected to an FSM, mapped by MAC address. & Once per self-test & on request & MNC \\
& & & \\
Data valid flag & 2 bits, one for each polarisation. A bit shall be 1 by default, and set to 0 if the incoming data is valid. & As fast as feasible, at least 1 Hz & Immediate update & Antennas/ASP \\
& & & \\
Actual pointing & Antenna pointing, mapped by ID & 1 Hz & Immediate update & Antennas (responding to OPL) \\
& & & \\
Beamformer coordinates & Desired coordinates for synthesized beam placement & 1 Hz & Immediate update & OPL \\
& & & \\
Telescope operating mode & Current global state of the DSA & 1 Hz & Immediate update & MNC \\
& & & \\
MNC commands & Commands issued to RCF by MNC & - & Immediate update & MNC \\
& & & \\
Zoom center frequencies & Center frequencies for the zoom modes & 1 Hz & Immediate update & OPL \\
\bottomrule
\end{tabular}
\caption{Basic high-level metadata requirements. While their ultimate origins may differ, these metadata will be delivered to RCF from the SAC (with the possible exception of high-cadence updates, such as those for the data valid flag).}
\label{tab:metadata-requirements}
\end{table*}

In addition to the above, it is expected that the MNC framework will be capable of carrying and delivering internal RCF messages at scale across all FSMs and SRMs servicing all 2048 antennas. These include control and monitoring messages exchanged between the coordinator and all SRMs and FSMs, and also those (for example) in Appendix \ref{app:fsm-monitoring}).

\footnotetext[1]{Each of these items should be accompanied by a timestamp corresponding to when the data was last updated.}

## Design Overview
\label{sec:overview}

RCF is controlled by a central coordinator process which provides a direct interface with MNC, translating specified observing requirements and commands (from e.g. the SAC) into commands issued to internal RCF components (primarily, FPGA station modules responsible for channelisation and beamforming). This approach keeps the complexity of the processes running on the FPGA station modules (FSMs) to a minimum, and prevents the duplication of metadata traffic with MNC. Internal RCF logic and operations are easily modified, developed and maintained behind the interface without affecting other telescope subsystems.

The coordinator responds to overall telescope state transitions and ensures that the RCF components operate in accordance with the requirements of each state. It also aggregates and delivers high-level self-test information when required by MNC.

During normal operations, the coordinator instantiates and maintains internal state machines corresponding with each group of antennas participating in a distinct observation or procedure (e.g. maintenance), allocating RCF resources and partitioning metadata and requests as needed. The coordinator also imposes any necessary restrictions on these groups, related, for example, to beamforming (see Section \ref{sec:beamformer-fsm}).

## Observing Sequence
\label{sec:obs-sequence}

The following describes the sequence of events that takes place during a standard survey observation with a single group of antennas, beginning with the telescope in the OFF state described in [@sys-des]. For full details on the coordinator, FSMs and SRMs, see Sections \ref{sec:coordinator}, \ref{sec:fsm} and \ref{sec:srm}.

\begin{enumerate}

\item The coordinator service (Section \ref{sec:coordinator}) is turned on, and begins listening to a set of MNC topics/channels for incoming commands (e.g. configure, deconfigure and track pointing, among others).

\item A telescope command to transition to the SAFE state is received by the coordinator.

\item The coordinator then turns on all 2048 FSMs (Section \ref{sec:fsm}) via the SRMs (Section \ref{sec:srm}) which provide IPMI-like out-of-band power control. It listens to response topics/channels for each FSM at which their individual self-test results will be delivered. It continues to listen to the MNC topics described above, waiting for further commands.

\item The FSMs' control and monitoring processes program the FPGAs from firmware binaries previously delivered to the FSMs.

\item The FSMs' control and monitoring processes configure the FPGA peripherals (ADCs, clock generators, etc) via config information previously delivered and stored locally.

\item The FSMs' control and monitoring processes then initiate a system self test, and report the results via the self-test monitoring topics to the coordinator.

\item The coordinator aggregates and delivers the system self test results to MNC, via a specified response topic.

\item OPL delivers observation metadata and parameters to the MNC subarray controller, which requests a transition to an observing mode, SURVEY, MANUAL or DEGRADED.

\item The coordinator receives a command (e.g. configure) to transition into an observing mode, and receives the parameters associated with the current recording. These include the list of participating antennas, the current antenna pointing, the current beamformer coordinates, the zoom mode center frequencies, etc.

\item The coordinator instantiates a state machine to manage observing for the subarray specified by the parameters from the specific observing element above.

\item The coordinator instructs the FSMs corresponding with antennas participating in the current subarray to configure, and delivers them the associated metadata.

\item The FSMs' control and monitoring processes then configure the data path for the FPGAs:

\begin{enumerate}
    \item Load EQ and FFT scaling settings.
    \item Configure radio camera wideband channel selections.
    \item Configure zoom band selections.
    \item Receives calibration information from RCP via a monitoring point.
    \item Starts the delay calculation module and loads the calibration and tracking delays for the radio camera and beamformer.
\end{enumerate}

\item The coordinator defines mappings from IP address -> channel number -> frequency and makes these available via the RCF API.

\item The FSMs' control and monitoring processes then configure the outputs:

\begin{enumerate}
    \item Allocates frequency channels to packets (radio camera and beamformer).
    \item Allocates packet destination IPs as determined above.
    \item Turns on packet transmission.
\end{enumerate}

\item The FSMs then begin reporting signal path monitoring data back to the coordinator, via their response topics.

\item The FSMs continually receive and respond to pointing commands, beamformer coordinate changes, data validity flags and others.

\end{enumerate}

## Coordinator Process
\label{sec:coordinator}

This process will run on a virtualized server independently of the RCF hardware. It responds to high level telescope states [@sys-des] and will be responsible primarily for the following:

\begin{itemize}
\item Providing an interface for RCF (via MNC) to the SAC and other subsystems.
\item Collecting and collating monitoring data from all FSMs and SRMs, and delivering it to MNC (where collated monitoring products are needed)
\item High level control of the fleet of SRMs and FSMs in response to telescope-wide state
\item Providing a gateway for centralised manual control over the SRMs and their corresponding FSMs, to be used for debugging, diagnostics and operations when the telescope is in the MANUAL state.
\end{itemize}

In response to high-level telescope state transitions, it issues commands to the SRMs and FSMs and receives metadata from them. These command channels are generally accessible and can be used manually for debugging, etc.

Its state diagram is provided in Figure \ref{fig:coordinator-states} and state transition table in \ref{tab:coordinator-states}.

\begin{figure*}
    \centering
    \includegraphics[width=\textwidth]{figures/coordinator-states.drawio.pdf}
    \caption{State transition diagram for the coordinator.}
    \label{fig:coordinator-states}
\end{figure*}

During normal operations, the coordinator will spend most of its time in ALLOCATE. In this state, the coordinator allocates resources (i.e. FSMs and SRMs) to new subarrays as specified by OPL (via MNC). For each active subarray, it instantiates a separate state machine to govern its operations (see \ref{fig:subarray-states}).

\begin{figure*}
    \centering
    \includegraphics[width=\textwidth]{figures/subarray.drawio.pdf}
    \caption{State transition diagram for individual subarrays as instantiated by the coordinator.}
    \label{fig:subarray-states}
\end{figure*}

\begin{table*}[htbp]
\centering
\footnotesize
\begin{tabular}{P{0.13\textwidth} P{0.25\textwidth} P{0.25\textwidth} P{0.25\textwidth}}
\toprule
\textbf{State} & \textbf{Entry conditions} & \textbf{Actions on entry} & \textbf{Exit conditions} \\
\midrule

ON & Manual control & 
Starts up the coordinator & 
Automatically to ALLOCATE or by manual control into MANUAL\\

& & & \\

ALLOCATE & 
Automatically from START when the telescope begins any form of observation or enters any observing state.
& Allocates FSMs to a new group corresponding with the antennas participating in the current observation, and instantiates a group-specific state machine to deliver further instructions and metadata to the participating resources &
\begin{itemize*}[itemjoin={\newline}]
\item To ON when the telescope leaves any of the observing states
\item To STOP if instructed by MNC 
\end{itemize*}
\\

& & & \\

STOP & 
Command from MNC
&  Instructs all remote processes to halt gracefully, and shuts down FSMs before stopping the coordinator itself.
&
Automatically to OFF
\\

& & & \\

OFF & 
 From STOP automatically
& None &
Powered up by MNC\\

\bottomrule
\end{tabular}
\caption{Coordinator states, their entry and exit conditions, and their actions on entry.}
\label{tab:coordinator-states}
\end{table*}

### RCF external API
\label{sec:external-api}

RCF's external API, made available to other subsystems via MNC's framework, is supplied by the coordinator process. This section provides a basic initial concept for this API (see Table~\ref{tab:rcf-interface}) and will be updated and extended during development. See also Tables~\ref{tab:metadata-requirements} and~\ref{tab:fsm-interface}.

\begin{table*}[htbp]
\centering
\footnotesize
\begin{tabular}{P{0.26\textwidth} P{0.08\textwidth} P{0.08\textwidth} P{0.08\textwidth} P{0.5\textwidth}}
\toprule

Key & Delivery & Origin & Type & Description \\
\midrule
array:<array ID> & push & received & dict & dictionary of antennas participating in a single coordinated observation.\\
beams:<array ID> & push & received & dict & dictionary of beam coordinates (RA and Dec) requested for synthesized beamforming.\\
zoom:<array ID> & push & received & dict & dictionary of zoom-mode center frequencies requested by the observer.\\
pointing:<array ID> & push & received & tuple & pointing coordinates for all antennas in <array ID> (RA and Dec).\\
pointing:<antenna ID> & push & received & tuple & pointing coordinates for a specific antenna, for e.g. maintenance or beam pattern measurements.\\
state:<array ID> & push & received & string & global array state (OFF, SAFE, SURVEY, DEGRADED or MANUAL).\\
valid:<antenna ID> & push & received & int & data validity flag, 1 bit per polarisation, for a specific antenna. Upstream services/produces shall set these to 1 if data becomes invalid for any reason and 0 while the data remain valid.\\
\midrule
rcf-selftest & push & sent & dict & results of system self-test: global number of antennas that RCF can process. Includes timestamp of last self-test. Includes result for each individual antenna.\\
rcf-status:<antenna> & written & sent & bool & single value, continuously updated, which indicates whether all the RCF components associated with a specific antenna are functioning nominally.\\
rcf-state & key & written & string & global RCF system state w.r.t. required state transitions. Values are ON, ALLOCATE and STOP.\\
rcf-state:<array ID> & key & written & string & state of a group of antennas participating in a single coordinated observation.\\
beam-status:<array ID> & key & written & dict & status of each requested beam.\\

\bottomrule
\end{tabular}
\caption{Initial basic concept for RCF's externally-facing API provided by the coordinator process.}
\label{tab:rcf-interface}
\end{table*}


## FPGA Station Module Process
\label{sec:fsm}

Each FSM will run its own individual lightweight control and monitoring process, illustrated in Figure \ref{fig:processes}. They will operate in an online streaming fashion with internal state kept to a minimum. Where possible, complexity is confined to the coordinator process.

\begin{figure*}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/processes.pdf}
    \caption{Each FSM hosts its own control and monitoring process. Global monitoring and control is governed by the coordinator process, and is used sparingly in limited circumstances.}
    \label{fig:processes}
\end{figure*}

The FSM states are described by a relatively simple diagram (Figure \ref{fig:fsm-states}). The transitions between these states are outlined in Table \ref{tab:fsm-states}. 

\begin{figure*}
    \centering
    \includegraphics[width=\textwidth]{figures/FSM-states.drawio.pdf}
    \caption{State transition diagram for the FPGA station modules.}
    \label{fig:fsm-states}
\end{figure*}

\begin{table*}[htbp]
\centering
\footnotesize
\begin{tabular}{P{0.13\textwidth} P{0.25\textwidth} P{0.25\textwidth} P{0.25\textwidth}}
\toprule
\textbf{FSM state} & \textbf{Entry conditions} & \textbf{Actions on entry} & \textbf{Exit conditions} \\
\midrule

START & Automatically on being booted by its SRM & 
\begin{enumerate*}[itemjoin={\newline}]
\item Start up control and monitoring process
\item Retrieve metadata
\end{enumerate*} & 
Automatically to TEST\\

& & & \\

TEST & 
\begin{itemize*}[itemjoin={\newline}]
\item Automatically from START 
\item If the SRM requests a self-test
\end{itemize*}
& Conducts a system self test & 
\begin{itemize*}[itemjoin={\newline}]
\item To ERROR if tests fail
\item To PROCESS if tests pass
\end{itemize*}
\\

& & & \\

PROCESS & 
If self-tests pass
& Process incoming data & 
\begin{itemize*}[itemjoin={\newline}]
\item Unrecoverable error
\item Turn-off command from SRM
\end{itemize*}
\\

& & & \\

STOP & 
Command from Coordinator
& \begin{enumerate*}[itemjoin={\newline}]
\item Halt processing
\item Halt control and monitoring process
\item Power down safely
\end{enumerate*}  &
Automatically to OFF
\\

& & & \\

ERROR & 
\begin{itemize*}[itemjoin={\newline}]
\item Failed system self-test
\item Error during PROCESS
\end{itemize*}
 & Halt any processing & Manual intervention or STOP command from Coordinator \\

& & & \\

OFF & 
\begin{itemize*}[itemjoin={\newline}]
\item From STOP automatically
\item Powered down by SRM
\end{itemize*}
& None & 
Powered up by SRM
\\

\bottomrule
\end{tabular}
\caption{FSM states, their entry and exit conditions, and their actions on entry.}
\label{tab:fsm-states}
\end{table*}

On startup, the control and monitoring process for an individual FSM will determine its location in the signal chain (such as identifying the antenna it is connected to via e.g. its MAC address) and begin retrieving appropriate metadata. This includes the four synthesized beam coordinates for beamforming. Note that only four synthesized beam coordinates may be specified per rack (and its associated antennas).

It will perform a self-test and report the results to the coordinator (in accordance with the requirements in [@sys-des]). If there are no errors, it will automatically enter the PROCESS state.

When processing (corresponding with the global telescope states of SURVEY, DEGRADED and MANUAL [@sys-des]) the FSM control and monitoring process will do the following:

\begin{itemize}
\item Retrieve the appropriate delays and deliver them to the delay control module at 1 second intervals
\item Retrieve calibration solutions from RCP and deliver them to the delay control module
\item Instruct the FPGA to blank outgoing data if the incoming data are flagged as invalid via any MNC-provided monitoring points
\item Perform continuous health monitoring and logging
\item Respond to commands from the Coordinator.
\end{itemize}

Tables \ref{tab:fsm-monitoring}, \ref{tab:fpga-monitoring} and \ref{tab:signal-monitoring} in the appendix enumerate the various montoring points for the FSMs (see also the [spreadsheet](https://docs.google.com/spreadsheets/d/1i-lr3it-rlygdl-o1LTDPPCw-QtcPXna0VmHqSmzSMA/edit?gid=1520511033#gid=1520511033)).

### FSM internal interface
\label{sec:external-api}

RCF will have access to an extensive internal interface, both for control via the coordinator and for development. Besides the interface described in Table~\ref{tab:fsm-interface} and the FSM monitoring points described in \ref{app:fsm-monitoring}, out-of-band IPMI-like control is available via the SRMs (see Section~\ref{sec:srm}).

\begin{table*}[htbp]
\centering
\footnotesize
\begin{tabular}{P{0.26\textwidth} P{0.08\textwidth} P{0.08\textwidth} P{0.08\textwidth} P{0.5\textwidth}}
\toprule

Key & Delivery & Origin & Type & Description \\
\midrule

shutdown & push & received & dict & instruction to halt FSM operations and processes in an orderly fashion for a set of specific FSMs.\\
fsm-state:<antenna ID> & push, written & sent & string & state of a particular FSM.\\
flag:<antenna ID> & push & received & int & instruction to flag outgoing packets as invalid.\\
fsm-test & push & received & dict & instruction for a set of FSMs to conduct a system self-test.\\
fsm-results:<antenna ID>  & push, written & sent & dict & results of FSM self test (format of results TBD).\\
firmware-location & key & read & string & path to firmware updates (locally per FSM).\\
firmware-update & push & received & dict & instruction to update firmware for specific FSMs by antenna ID.\\

\bottomrule
\end{tabular}
\caption{Subset of the conceptual internal interface; see \ref{app:fsm-monitoring} for additional internal monitoring point details.}
\label{tab:fsm-interface}
\end{table*}


### Firmware updates
\label{sec:firmware updates}

Each firmware update is expected to be around 30 MB and will be provided infrequently. There are several options available for delivering firmware updates to the FSMs. They could be delivered via MNC transport if this is feasible performance-wise; or alternatively, the location of the update could be provided via MNC (and the update delivered separately outside of MNC).

### Beamformer FPGA Station Module Process
\label{sec:beamformer-fsm}

While the beamformer FSMs will adhere to the state diagram in Figure \ref{fig:fsm-states}, they will require a separate control and monitoring process, since they perform a different set of tasks. These processes will retrieve the beam coordinates processed by each rack's subarray for combining.

Due the the processing mechanism outlined in  [@rcf-des], synthesized beams may only be formed on four unique coordinate pairs per rack. Unique synthesized beams are effectively confined to subarrays made up of multiples of 80 antennas. Each rack processes the data from 80 antennas.

## Subrack Management Cards
\label{sec:srm}

The SRMs do not run a full operating system and will be used for simple high-level control over the FSMs in a similar fashion to out-of-band mechanisms such as IPMI.

When ON, they will offer the following operations in response to queries:

\begin{itemize}

\item Report the power status of the FSMs to which it is connected
\item Report the power status of a specific FSM to which it is connected
\item Power up a specific FSM
\item Power down a specific FSM
\item Power cycle a specific FSM
\item Report basic status about itself (nominal functioning)

\end{itemize}


