# Introduction
\label{sec:introduction}

The DSA-2000 is a large array with near-complete UV coverage, designed to survey the entire visible sky repeatedly over a five year period. Primarily a survey telescope, it will spend the vast majority of its time conducting these automated all-sky surveys.

The telescope comprises 14 different subsystems described in Section 3.4 of [@sys-des]. RCF, the Radio Camera Frontend, is responsible for digitising analog signals from each antenna, upchannelising the digitised signals and delivering the associated data products via Ethernet to RCP, the Radio Camera Processor. These include a wideband data product and two "zoom" products, with user-selectable center frequencies (see Section 3.6 of [@rcf-des]). RCF is also responsible for forming up to 4 concurrent synthesized beams and delivering these products to the Pulsar Timing subsystem.

At a hardware level, RCF must digitise and process 2 RF signals from each of the 2048 antennas in the array, delivered to a central processing facility by the Analog Signal Path subsystem (ASP). To keep complexity to a minimum, each dual-polarisation signal pair from a single antenna is processed by a separate ``FPGA Station Module" (FSM). These FSMs are managed in groups of 10 by Subrack Management Cards or SRMs, which provide IPMI-like out-of-band management control over the connected FSMs. Further details of the hardware and architecture are provided in [@rcf-des].

Optimising for large-scale automated survey operations, RCF will operate in an online streaming manner, running continuously and processing incoming data to the maximum extent possible while flagging invalid outgoing data. However, RCF must also respond to global telescope state transitions (described in Table 1 of [@sys-des]) and perform required actions such as a system self-test when required.

RCF must operate along with the rest of the subsystems in several different observing modes. See Section \ref{sec:obs-sequence} for greater detail pertaining to RCF's internal monitoring and control operations during an observation. In very high-level terms, however, an observation takes place as follows:

\begin{enumerate}
\item On startup the telescope performs a self-test, which is requested of subsystems via MNC. RCF performs a system self test and reports the results via MNC.
\item OPL delivers items to the observing queue, specifying such metadata as the pointing coordinates, list of participating antennas, center frequencies of zoom bands A and B, coordinates for synthesized beamforming, etc.
\item Elements from the queue are executed by the MNC subarray controller (SAC), and RCF receives metadata and instructions for the current element.
\item RCF coordinates and configures the required FSMs, which digitise the incoming data, upchannelise them, perform beamforming and deliver the associated data products to RCP and PT.
\item During the recording, RCF reports monitoring data and listens for updates in the metadata provided, flagging outgoing data as needed.
\end{enumerate}

RCF must be able to perform these tasks simultaneously across separate groups of antennas. This includes offering manual control over the hardware and processing for individual antennas, which might e.g. be undergoing maintenance or debugging.

## Assumptions and Requirements
\label{sec:requirements}

RCF will provide a standardised API for the MNC SAC [@mnc-des] and other subsystems to interact with. It is expected that this API will be provided via the MNC transport layer. The general MNC message schema and transport mechanism will need to be agreed upon, and the RCF API will specify e.g. commands and the actual metadata required.

It is assumed that commands and metadata directed to RCF will come directly from the MNC subarray controller. For example, OPL will specify all the parameters of an observation including pointing coordinates and center frequencies of the ``zoom" products, etc. Using an interface library provided by MNC, it will transmit (and update) these parameters via MNC transport to the MNC SAC. The metadata required by RCF originates from OPL (or other subsystems as needed), but the actual formatting and delivery (to RCF) is assumed to be handled by the SAC. These assumptions and others can be summarised below:

\begin{itemize}

\item RCF will generally communicate with the SAC, rather than with other subsystems directly.

\item The SAC will dictate which antennas should be included in the current subarray, but will not take on low-level control of subsystem hardware and components.

\item Communications will mostly follow a command-response format, wherein the SAC will issue a command to RCF, and RCF will send responses (there could be several) back.

\item RCF (and other subsystems) will provide an external API implementing command functions (e.g. "configure" or "track pointing"). Underneath, these commands will be transmitted via the common MNC transport layer.

\item Communications between internal RCF components will be handled separately from the externally-facing API and will not be exposed (though they will still make use of the MNC transport layer and message schema).

\item For the moment, it is assumed we will be using Kafka for message transport and protobufs or capnproto for message serialising (though this may change as MNC develops).

\end{itemize}

RCF's internal operations must be managed rigorously across separate groups of antennas, ensuring precise delivery of correct metadata and timeous flagging of outgoing data products. In short, proper monitoring and control for RCF will:

\begin{enumerate}
\item Provide an external interface via MNC to the SAC.
\item Enable centralised low-level control of all RCF hardware for development and maintenance, permitting e.g. easy firmware updates to the FSMs and out-of-band, IPMI-like power control.
\item Autonomously and continuously control the hardware responsible for separate groups of antennas in response to the global state of the telescope, delivered via commands via the external interface.
\end{enumerate}

These can be enumerated further into a set of requirements, summarised in Table \ref{tab:requirements}.

\begin{table*}[htbp]
\centering
\footnotesize
\begin{tabular}{P{0.3\textwidth} P{0.7\textwidth} }
\toprule
Requirement & Description \\
\midrule
\textit{RcfCoordR-0001} & Continuous operation: Following startup procedures, RCF shall operate in a continuous manner, processing incoming data to the maximum extent possible. \\
\textit{RcfCoordR-0002} & Autonomous operation: The RCF controller shall respond automatically to the SAC (and other subsystems if required) and nominal operation shall not require human intervention. \\
\textit{RcfCoordR-0003} & Consistent external API: A consistent interface shall be made available to the SAC. Internal changes to the RCF controller will not affect this interface.\\
\textit{RcfCoordR-0004} & The RCF controller shall scale to handle up to 7 separate groups of antennas, including (for example) groups observing actively and independently from one another, or undergoing maintenance. \\
\textit{RcfCoordR-0005} & Asynchronous operation: The RCF controller must be able to respond to asynchronous commands and updates delivered via the API mentioned in \textit{RcfCoordR-0003}. \\
\textit{RcfCoordR-0006} & Responsiveness: The RCF controller must respond to external updates and commands within 0.5 seconds.\\
\textit{RcfCoordR-0007} & If RCF receives any indication (via MNC) that the incoming data are invalid, RCF shall begin flagging or blanking the outgoing data within 100ms after receiving an indication of invalid data. \\
\textit{RcfCoordR-0008} & Metadata delivery: The RCF controller shall collect and disseminate all the required metadata to each FSM as observations start and progress.\\
\textit{RcfCoordR-0009} & Remote debugging: Development and debugging must be possible system-wide. The interfaces required for this need not be kept consistent in the same manner as the external API.\\
\textit{RcfCoordR-0010} & Self-testing: The RCF controller must be able to perform basic system health checks across the entire fleet of FSMs, aggregate the results and make them available to other subsystems via the API in \textit{RcfCoordR-0003}. \\
\bottomrule
\end{tabular}
\caption{Summary of requirements for the RCF controller}
\label{tab:requirements}
\end{table*}
